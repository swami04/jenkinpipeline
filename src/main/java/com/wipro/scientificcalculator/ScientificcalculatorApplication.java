package com.wipro.scientificcalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScientificcalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(ScientificcalculatorApplication.class, args);
	}

}
