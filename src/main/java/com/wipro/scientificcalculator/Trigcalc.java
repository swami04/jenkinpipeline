package com.wipro.scientificcalculator;

public class Trigcalc {
	public double getSinValue(int degree) {
		double radians= Math.toRadians(degree);	
		double sinValue=Math.sin(radians);
		return sinValue;
		}
	 public static void main(String[] args) {
	        Trigcalc calculator = new Trigcalc();
	        
	       
	        System.out.println("Sin(0 degrees) = " + calculator.getSinValue(0));
	        System.out.println("Sin(30 degrees) = " + calculator.getSinValue(30));
	        System.out.println("Sin(90 degrees) = " + calculator.getSinValue(90));
	    }

}

